<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;


use App\Models\Status;

class StatusSeeder extends Seeder
{

    private $statuses = [
        [
            'name' => 'Active',
            'key' => 'active'
        ],
        [
            'name' => 'Draft',
            'key' => 'draft'
        ],
        [
            'name' => 'Inactive',
            'key' => 'inactive'
        ],
        [
            'name' => 'Deactivated',
            'key' => 'deactivated'
        ],
        [
            'name' => 'Suspended',
            'key' => 'suspended'
        ],
        [
            'name' => 'Pending',
            'key' => 'pending'
        ]

    ];

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->statuses as $item) {

            $status = Status::firstOrCreate(
                ['key' => $item['key']],
                [
                    'key' => $item['key'],
                    'name' => $item['name']
                ]
            );

        }
    }
}
