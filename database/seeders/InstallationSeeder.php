<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class InstallationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->call([
            CategorySeeder::class,
            TypeSeeder::class,
            StatusSeeder::class,
            EntitySeeder::class,
            EntityActivityAccessSeeder::class,
            AccessLevelSeeder::class,
            UserSeeder::class,
        ]);
    }
}
