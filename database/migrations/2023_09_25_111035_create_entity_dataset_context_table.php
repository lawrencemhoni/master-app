<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('entity_dataset_context', function (Blueprint $table) {
            $table->id();
            $table->integer('entity_id');
            $table->integer('type_id');
            $table->integer('instance_id');
            $table->integer('dataset_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('entity_dataset_context');
    }
};
