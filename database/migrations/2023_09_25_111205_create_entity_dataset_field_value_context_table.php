<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('entity_dataset_field_value_context', function (Blueprint $table) {
            $table->id();
            $table->integer('entity_id');
            $table->integer('type_id');
            $table->integer('instance_id');
            $table->integer('dataset_id');
            $table->integer('field_id');
            $table->string('text_value');
            $table->decimal('decimal_value');
            $table->datetime('number_value');
            $table->datetime('date_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('entity_dataset_field_value_context');
    }
};
