<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\Type;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AccessLevel>
 */
class AccessLevelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $type = Type::filterWithCategoryKey('access_level_types_category')
                    ->first();
        return [
            'name' => fake()->name(),
            'type_id' => $type->id
            //
        ];
    }
}
