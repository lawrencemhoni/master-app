<?php

namespace Tests;

use Artisan;
use App\Models\User;
use App\Models\EntityActivityAccess;


use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $user;

    public function setUp () : void
    {

        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed', ['--class' => 'InstallationSeeder']);

        $this->user = User::factory()->create();

        $accesses = EntityActivityAccess::select()->pluck('id')->toArray();

        $this->user->entityActivityAccesses()->attach($accesses);

    }

    public function actingAsTestUser () {
        return $this->actingAs($this->user);
    }







}
