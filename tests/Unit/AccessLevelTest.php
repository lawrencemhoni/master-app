<?php

namespace Tests\Unit;

use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;

use App\Models\Type;
use App\Models\AccessLevel;
use App\RequestModelManagers\AccessLevelManager;


class AccessLevelTest extends TestCase
{
    use DatabaseMigrations;


    /**
     * Create Access Level Test
     * @test
     */
    public function  user_can_create_access_level () : void
    {

        $type = Type::filterWithCategoryKey('access_level_types_category')
                    ->first();

        $data = [
            'name' => 'Super Admin',
            'type_id' => $type->id
        ];

        $req = Request::create('/create', 'POST', $data);

        $accessLevel = AccessLevelManager::createAccessLevel($req);

        $this->assertDatabaseHas('access_levels', $data);

    }

    /**
     * Get Access Levels Test
     * @test
     */
    public function user_can_get_access_levels_test () : void
    {

        $req = Request::create('/access-levels', 'GET');

        $accessLevels = AccessLevelManager::getAccessLevels($req);

        foreach ($accessLevels as $accessLevel) {

            $this->assertDatabaseHas('access_levels', [
                'id' => $accessLevel->id,
                'name' => $accessLevel->name,
                'type_id' => $accessLevel->type_id
            ]);

        }

    }

    /**
     * Get Access Level Test
     * @test
     */
    public function user_can_get_access_level () : void
    {
        $accessLevel = AccessLevel::factory()->create();

        $req = Request::create('/access-level', 'GET');

        $foundAccessLevel = AccessLevelManager::getAccessLevel($req, $accessLevel->id);

        if ($foundAccessLevel->id == $accessLevel->id &&
            $foundAccessLevel->name == $accessLevel->name &&
            $foundAccessLevel->type_id == $accessLevel->type_id) {

            $this->assertTrue(true);

        } else {
            $this->assertTrue(false);
        }

    }



    /**
     * Update Access Level Test
     * @test
     */
    public function user_can_update_access_level () : void
    {
        $accessLevel = AccessLevel::factory()->create();

        $data = [
            'name' => 'Special Admin',
            'type_id' => $accessLevel->type_id
        ];

        $req = Request::create('/update', 'POST', $data);

        $updatedAccessLevel = AccessLevelManager::updateAccessLevel($req, $accessLevel->id);

        if ($updatedAccessLevel->id == $accessLevel->id &&
            $updatedAccessLevel->name == $data['name'] &&
            $updatedAccessLevel->type_id == $accessLevel->type_id) {

            $this->assertTrue(true);

        } else {
            $this->assertTrue(false);
        }

    }







}
