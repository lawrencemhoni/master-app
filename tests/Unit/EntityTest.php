<?php

use Tests\TestCase;

use Illuminate\Http\Request;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Entity;

use App\RequestModelManagers\EntityManager;

class EntityTest extends TestCase
{
    use DatabaseMigrations;

   /**
    * Get Entities Test
    * @test
    */
    public function user_can_get_entities () : void
    {

        $req = Request::create('/user', 'GET');

        $foundEntities= EntityManager::getEntities($req);

        foreach ($foundEntities as $entity) {
            $this->assertDatabaseHas('entities', [
                'name' => $entity->name,
                'key' => $entity->key
            ]);
        }

    }
}
