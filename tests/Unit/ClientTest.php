<?php

namespace Tests\Unit;

use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;

use App\Models\Client;
use App\RequestModelManagers\ClientManager;

class ClientTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Create Client Test
     * @test
     */
     public function user_can_create_client () : void
     {
        $data = [
            'name' => 'Peza android',
            'type_key' => 'android_mobile_app_type'
        ];

        $req = Request::create('/create', 'POST', $data);

        $client = ClientManager::createClient($req);

        unset($data['type_key']);

        $this->assertDatabaseHas('clients', $data);
     }


     /**
      * Get clients test
      * @test
      */
     public function user_can_get_clients () : void
     {

        Client::factory()->count(10)->create();

        $req = Request::create('/clients', 'GET');

        $clients = ClientManager::getClients($req);

        foreach ($clients as $client) {
            $this->assertDatabaseHas('clients', [
                'id' => $client->id,
                'name' => $client->name,
                'type_id' => $client->type_id
            ]);
        }

     }

     /**
      * Get client test
      * @test
      */
      public function user_can_get_client () : void
      {
         $client = Client::factory()->create();

         $req = Request::create('/clients', 'GET');

         $foundClient = ClientManager::getClient($req, $client->id);

         if ($foundClient->id == $client->id && $foundClient->name == $client->name) {
            $this->assertTrue(true);
         }

      }

      /**
       * Update client test
       * @test
       */
      public function user_can_update_client () : void
      {

        $client = Client::factory()->create();

        $newData = [
            'name' => 'Ndi Deal IOS Client'
        ];

        $newData['name'] = 'All things IOS App';
        $newData['type_key'] = 'ios_mobile_app_type';

        $req = Request::create('/create', 'POST', $newData);

        $newClient = ClientManager::updateClient($req, $client->id);

        unset($newData['type_key']);

        $this->assertDatabaseHas('clients', $newData);

      }





}
