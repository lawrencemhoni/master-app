<?php

namespace Tests\Feature;

use Tests\TestCase;

use Illuminate\Testing\Fluent\AssertableJson;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\User;

class UserTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * Create User Test
     * @test
     */
    public function user_can_create_user () : void {

        $url = 'platform-zero/users/create';

        $data = [
            'username' => 'larry',
            'first_name' => 'Lawrence',
            'last_name' => 'Mhoni',
            'email' => 'lawrencemhoni@gmail.com',
            'access_level_id' => 1,
            'password' => 'abc123'
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response
            ->assertStatus(201)
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('data.username', $data['username'])
                    ->where('data.first_name', $data['first_name'])
                    ->where('data.access_level_id', $data['access_level_id'])
                    ->where('data.email', fn (string $email) => str($email)->is($data['email']))
                    ->missing('password')
                    ->etc()
        );

    }

    /**
     * Get User Test
     * @test
     */
    public function user_can_get_user() : void
    {

        $user = User::factory()->create();

        $url = "/platform-zero/users/{$user->id}";

        $response = $this->actingAsTestUser()->getJson($url);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('data.id', $user->id)
                 ->where('data.username', $user->username)
                 ->where('data.email', fn (string $email) => str($email)->is($user->email))
                 ->missing('password')
                 ->etc()
        );


    }

    /**
     * Get Users Test
     * @test
     */
    public function user_can_get_users () : void
    {

        User::factory()->count(20)->create();

        $url = '/platform-zero/users';

        $response = $this->actingAsTestUser()->getJson($url);


        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'username',
                    'first_name',
                    'last_name'
                ]
            ]
        ]);

    }

    /**
     *Update User Test
     *@test
     */
    public function user_can_update_user () : void
    {
        $user = User::factory()->create();

        $url = "/platform-zero/users/{$user->id}/update";

        $data = [
            'username' => 'ShaneM',
            'first_name' => 'Shane',
            'last_name' => 'Moyo',
            'email' => 'shane@gmail.com',
            'access_level_id' => 1,
            'password' => 'abc123'
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('data.username', $data['username'])
                    ->where('data.first_name', $data['first_name'])
                    ->where('data.access_level_id', $data['access_level_id'])
                    ->where('data.email', fn (string $email) => str($email)->is($data['email']))
                    ->missing('password')
                    ->etc()
        );
    }


}
