<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Illuminate\Testing\Fluent\AssertableJson;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Request;

use App\Models\Entity;
use App\RequestModelManagers\EntityManager;


class EntityTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * GET Entities Test
     * @test
     */
    public function user_can_get_entities () : void
    {

        $url = '/platform-zero/entities';

        $response = $this->actingAsTestUser()->getJson($url);


        $response->assertStatus(200)
                 ->assertJsonStructure([
                    'data' => [
                        [
                            'id',
                            'name'
                        ]
                    ]
                ]);


    }


}
