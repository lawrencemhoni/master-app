<?php

namespace Tests\Feature;

use Tests\TestCase;

use Illuminate\Testing\Fluent\AssertableJson;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\App;

class AppTest extends TestCase
{

    use DatabaseMigrations;

    /**
     * Create App Test
     * @test
     */
    public function user_can_create_app () : void
    {

        $url = 'platform-zero/apps/create';

        $data = [
            'name' => 'Thola Android App',
            'type_key' => 'android_mobile_app_type'
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response->assertStatus(201)
                 ->assertJson(fn (AssertableJson $json) =>
                    $json->where('data.name', $data['name'])
                        ->etc()
                );

    }

    /**
     * Get Platform Test
     * @test
     */
    public function user_can_get_app() : void
    {

        $app = App::factory()->create();

        $url = "/platform-zero/apps/{$app->id}";

        $response = $this->actingAsTestUser()->getJson($url);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('data.id', $app->id)
                 ->where('data.name', $app->name)
                 ->etc()
        );


    }


   /**
     * Get Apps Test
     * @test
     */
    public function user_can_get_apps () : void
    {

        App::factory()->count(20)->create();

        $url = '/platform-zero/apps';

        $response = $this->actingAsTestUser()->getJson($url);


        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name'
                ]
            ]
        ]);

    }




   /**
     *Update App Test
     *@test
     */
    public function user_can_update_app () : void
    {
        $app = App::factory()->create();

        $url = "/platform-zero/apps/{$app->id}/update";

        $data = [
            'name' => 'Pretty Treasures Blankets',
            'type_key' => 'android_mobile_app_type'
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('data.name', $data['name'])
                    ->etc()
        );
    }














}
