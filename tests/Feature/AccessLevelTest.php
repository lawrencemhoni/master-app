<?php

namespace Tests\Feature;

use Tests\TestCase;

use Illuminate\Testing\Fluent\AssertableJson;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Type;
use App\Models\AccessLevel;

class AccessLevelTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Create Access Level Test
     * @test
     */
    public function user_can_create_access_level () : void
    {

        $url = 'platform-zero/access-levels/create';

        $type = Type::filterWithCategoryKey('access_level_types_category')
                    ->first();

        $data = [
            'name' => 'Some temp admin',
            'type_id' => $type->id
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response->assertStatus(201)
                 ->assertJson(fn (AssertableJson $json) =>
                    $json->where('data.name', $data['name'])
                        ->etc()
                );

    }



    /**
     * Get Access Levels Test
     * @test
     */
    public function user_can_get_access_levels () : void
    {

        AccessLevel::factory()->count(20)->create();

        $url = '/platform-zero/access-levels';

        $response = $this->actingAsTestUser()->getJson($url);

        $response->assertStatus(200)
                 ->assertJsonStructure([
                    'data' => [
                        [
                            'id',
                            'name'
                        ]
                    ]
                ]);

    }


    /**
     * Get Access Level Test
     * @test
     */
    public function user_can_get_access_level () : void
    {

        $accessLevel = AccessLevel::factory()->create();

        $url = "/platform-zero/access-levels/{$accessLevel->id}";

        $response = $this->actingAsTestUser()->getJson($url);

        $response->assertJson(fn (AssertableJson $json) =>
            $json->where('data.id', $accessLevel->id)
                 ->where('data.name', $accessLevel->name)
                 ->etc()
        );


    }


    /**
     *Update AccessLevel Test
     *@test
     */
    public function user_can_update_access_level () : void
    {
        $accessLevel = AccessLevel::factory()->create();

        $url = "/platform-zero/access-levels/{$accessLevel->id}/update";

        $data = [
            'name' => 'Platforms Manager',
            'type_id' =>  $accessLevel->type_id
        ];

        $response = $this->actingAsTestUser()->postJson($url, $data);

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('data.name', $data['name'])
                    ->etc()
        );
    }




}
