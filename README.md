## About Everything

Everything is an API that allows Developers and non Developers to build Applications by writing Less Code. For now the project is about Architectural and conceptual experiments and discoveries. 


## License

This is not an open source project. It belongs to Lawrence Mhoni.


## Installation



### Dependencies

- Docker and docker-compose



### Setting up with docker

First clone the repositories into your desired directory

```
git clone https://gitlab.com/lawrencemhoni/master-app.git

```

Navigate into the project's directory


```
cd everything

```


Now set up database credentials


```
cp .env.example .env

```

Edit the .env config file


```
nano .env

```



Change the following:

*Make sure DB_HOST=db it's very very important.

*But as for the ones in brackets, replace the brackets with whatever you wish and remove the surrounding brackets.


```
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=[database_name]
DB_USERNAME=[username]
DB_PASSWORD=[your_password]

```





Then build the docker images


```
docker-compose build app

```


Once the images have been built successfully run the following:


```
docker-compose up -d

```



You can confirm that the container is up and running by running the following:


```
docker-compose ps

```

### Setting up the Laravel application

Once you have the docker containers up and running while in the project directory, 
Go inside the container by running the following command;


```
docker-compose exec app bash

```

Once you are in the container, install all Laravel dependancies by running the following command.


```
composer install

```


Once dependancies have been installed, now generate application key

```
php artisan key:generate

```





Now let's do migration and install all the necessary data to get the application ready for use.

Run the following.


```
php artisan migrate --seed --seeder=InstallationSeeder && php artisan passport:install

```

You can test the application by running the following command.


```
php artisan test

```
