<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\EntityAccessService;

class EntityAccessServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->bind(EntityAccessService::class, function () {
            return new EntityAccessService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
