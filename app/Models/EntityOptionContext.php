<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntityOptionContext extends Model
{
    use HasFactory;
    protected $table = 'entity_option_context';
}
