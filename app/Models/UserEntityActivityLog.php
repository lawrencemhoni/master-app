<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserEntityActivityLog extends Model
{
    use HasFactory;

    protected $table = 'user_entity_activity_log';
}
