<?php


namespace App\Models;

use DB;

trait ExtensiveEloquence {

    public function scopeSelectBasics ($query) {

        $table  = $query->getQuery()->from;
        return $query->select(DB::raw("DISTINCT {$table}.id"), DB::raw("{$table}.*"));

    }

    public function scopeJoinIfNotJoined ($query, $table, $firstCol, $secondCol) {

        $joins = $query->getQuery()->joins;

        if($joins == null) {

            return $query->join($table, $firstCol, $secondCol);
        }

        foreach ($joins as $join) {
            if ($join->table == $table) {
                return $query;
            }
        }

        return $query->join($table, $firstCol, $secondCol);

     }

}
