<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class Entity extends Model
{
    use HasFactory;
    use ExtensiveEloquence;


    public function activityAccesses () {
        return $this->hasMany(EntityActivityAccess::class, 'entity_id')
                    ->addSelect(['type_name' => Type::select('name')
                                                    ->whereColumn('type_id', 'types.id')

                   ]);
    }


    public function activityTypes () {
        return $this->belongsToMany(Type::class, 'entity_activity_accesses', 'entity_id', 'type_id');
    }

    public function scopefilterWithTypeKey ($query, $typeKey) {

        return $query->joinIfNotJoined('types', 'types.id', 'entities.type_id')
                     ->where('types.key', $typeKey);

     }

     public function scopefilterWithTypeIdOrTypeKey ($query, $typeId, $typeKey) {


        $newQuery  = $query;

        $newQuery  = $newQuery->joinIfNotJoined('types', 'types.id', 'entities.type_id');

        if ($typeId) {

            return $newQuery->where('types.id', $typeId);

        } else if ($typeKey) {

            return $newQuery->where('types.key', $typeKey);

        }

        return $query;

     }


     public function scopefilterWithTypeIdsOrTypeKeys ($query, $typeIds, $typeKeys) {

        $newQuery  = $query;

        $newQuery  = $newQuery->joinIfNotJoined('types', 'types.id', 'entities.type_id');

        if ($typeIds) {

            return $newQuery->whereIn('types.id', $typeIds);

        } else if ($typeKeys) {

            return $newQuery->whereIn('types.key', $typeKeys);

        }

        return $query;


     }


}
