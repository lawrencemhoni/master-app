<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccessLevel extends Model
{
    use HasFactory;
    use ExtensiveEloquence;


    public function scopefilterWithTypeKey ($query, $typeKey) {

        return $query->joinIfNotJoined('types', 'types.id', 'access_levels.type_id')
                     ->where('types.key', $typeKey);

    }


}
