<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class Field extends Model
{
    use HasFactory;
    use ExtensiveEloquence;

    public function scopeDatasetIdHasFieldName($query, $datasetId, $fieldName) {

        $count = $query->select()
                       ->join('dataset_field', 'dataset_field.field_id', 'fields.id')
                       ->where('dataset_field.dataset_id', $datasetId)
                       ->where('fields.name', $fieldName)
                       ->count();

        return $count > 0;

    }

    public function options () {

        return $this->belongsToMany(
                    Option::class,
                    'entity_option_context',
                    'instance_id', 'option_id')
                    ->select(DB::raw('DISTINCT options.id'), 'options.name', 'options.type_id')
                    ->join('entities', 'entities.id', 'entity_option_context.entity_id')
                    ->join('types', 'types.id', 'entity_option_context.type_id')
                    ->where('entities.key', 'field')
                    ->where('types.key', 'instance_subject_capacity_type');

    }






}
