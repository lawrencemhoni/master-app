<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;
    use ExtensiveEloquence;



    /**
     * Find by category key
     */
    public function scopeFindByIdOrKey ($query, $typeId, $typeKey) {

        if ($typeId) {
            return $query->where('types.id', $typeId);
        } else if ($typeKey) {
            return $query->where('types.key', $typeKey);
        }

        return $query->where('types.key', null)->orWhere('types.id', null);
    }


    /**
     * Filter by category key
     */
    public function scopefilterWithIdOrKey ($query, $typeId, $typeKey) {

        if ($typeId) {
            return $query->where('types.id', $typeId);
        } else if ($typeKey) {
            return $query->where('types.key', $typeKey);
        }

        return $query;
    }


    public function scopefilterWithCategoryKey ($query, $catKey) {

        $newQuery = $query;

        if ($catKey) {
            return $newQuery->joinIfNotJoined('categories', 'categories.id', 'types.category_id')
                            ->where('categories.key', $catKey);
        }

        return $query;

     }


     public function scopefindWithCategoryKey ($query, $catKey) {

        return $query->joinIfNotJoined('categories', 'categories.id', 'types.category_id')
                        ->where('categories.key', $catKey);
     }



    public function scopefilterWithCategoryIdOrKey ($query, $categoryId, $categoryKey) {

        $newQuery = $query;

        $newQuery = $newQuery->joinIfNotJoined('categories', 'categories.id', 'types.category_id');

        if ($categoryId) {
            return $newQuery->where('categories.id', $categoryId);
        } else if ($categoryKey) {
            return $newQuery->where('categories.key', $categoryKey);
        }

        return $query;
    }

    public function scopefilterWithCategoryIdsOrKeys ($query, $categoryIds, $categoryKeys) {

        $newQuery = $query;

        $newQuery = $newQuery->joinIfNotJoined('categories', 'categories.id', 'types.category_id');

        if ($categoryIds) {

            return $newQuery->whereIn('categories.id', $categoryIds);

        } else if ($categoryKeys) {

            return $newQuery->whereIn('categories.key', $categoryKeys);
        }

        return $query;
    }




}
