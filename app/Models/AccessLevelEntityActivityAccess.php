<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccessLevelEntityActivityAccess extends Model
{
    use HasFactory;

    protected $table = 'access_level_entity_activity_accesses';
}