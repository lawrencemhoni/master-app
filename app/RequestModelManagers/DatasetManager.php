<?php

namespace App\RequestModelManagers;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Illuminate\Http\Request;
use App\Models\Type;
use App\Models\Option;
use App\Models\Entity;
use App\Models\Dataset;
use App\Models\Field;
use App\Models\EntityOptionContext;
use App\Exceptions\RecordConflictException;
use App\Exceptions\InvalidDataException;

use DB;


class DatasetManager extends RequestModelManager {

    /**
     * Create dataset
     * @param $req : Instance of the current request
     * @return Dataset : created dataset
     */
     public static function createDataset (Request $req) : Dataset
     {

        $type = self::getTypeByKeyOrFail($req->type_key);

        if (self::datasetExists($req->name, $type->id )) {
            self::throwRecordConflictException('Dataset', $req->name);
        }

        $dataset = new Dataset;
        $dataset->name = $req->name;
        $dataset->key = $req->key;
        $dataset->type_id = $type->id;
        $dataset->save();

        if (self::arrayHasItems($req->fields)) {

            self::attachDatasetFieldsFromArray($dataset, $req->fields);

        }

        return self::getDatasetWithRelationshipsByIdOrFail($dataset->id);
     }


    /**
     * Get datasets
     * @param $req : Instance of the current request
     * @return Dataset : a collection of datasets
     */
     public static function getDatasets (Request $req) : Paginator
     {

        $datasets = Dataset::select
                        (DB::raw('DISTINCT datasets.id'),
                        'datasets.name',
                        'datasets.key',
                        'datasets.type_id')
                        ->join('types', 'types.id', 'type_id');

        if ($req->type_key) {
            $datasets->where('types.key', $req->type_key);
        }

        if ($req->search_query) {
            $datasets->where('datasets.name', 'LIKE', "%%{$req->search_query}%%");
        }

        return $datasets->paginate();

     }


    /**
     * Get dataset
     * @param $req : Instance of the current request
     * @param $id : ID of the dataset
     * @return Dataset: found dataset
     */
     public static function getDataset (Request $request, $id) : Dataset
     {

        return self::getDatasetWithRelationshipsByIdOrFail($id);

     }


    /**
     * Update dataset
     * @param $req : Instance of the current request
     * @param $id : ID of the dataset
     * @return Dataset : updated dataset
     */
     public static function updateDataset (Request $req, $id) : Dataset
     {

        $type = self::getTypeByKeyOrFail($req->type_key);
        $dataset = self::getDatasetByIdOrFail($id);

        if (self::datasetExistsExceptForId($id, $req->name, $type->id)) {
            self::throwRecordConflictException('Dataset', $req->name);
        }

        $dataset = Dataset::select()
                          ->where('id', $id)
                          ->firstOrFail();

        $dataset->name = $req->name;
        $dataset->key = $req->key;
        $dataset->type_id = $type->id;
        $dataset->save();

        if (self::arrayHasItems($req->fields)) {

            self::attachDatasetFieldsFromArray($dataset, $req->fields);

        }

        return self::getDatasetWithRelationshipsByIdOrFail($id);

     }



     public static function attachDatasetFieldsFromArray ($dataset, $fieldsArray) {

        foreach ($fieldsArray as $fieldItem) {

            $fieldType;

            $typeCategoryKey = 'field_types_category';

            if (isset($fieldItem['type_key'])) {

                $fieldKey =  $fieldItem['type_key'];

                $fieldType = self::getCategoryTypeByKey($fieldKey, $typeCategoryKey);

                if (!$fieldType) {
                    continue;
                }

            } else if (isset($fieldItem['type_id'])) {

                $typeId = $fieldItem['type_id'];

                $fieldType = self::getCategoryTypeById($typeId, $typeCategoryKey);

                if (!$fieldType) {
                    continue;
                }
            }

            $field = null;

            if(isset($fieldItem['id'])) {
                $field = Field::select()
                             ->where('id', $fieldItem['id'])
                             ->first();

            } else if (self::datasetFieldExists($dataset->id, $fieldItem['name'] )) {
                continue;
            }

            $field = ($field)? $field : new Field;
            $field->name = $fieldItem['name'];
            $field->type_id = $fieldType->id;
            $field->save();

            if (!self::datasetHasField ($dataset->id, $field->id)) {
                $dataset->fields()->attach([$field->id]);
            }

            if (self::fieldItemHasOptions($fieldItem)) {
                self::attachFieldOptions($field, $fieldItem['options']);
            }

        }

     }


     public static function attachFieldOptions ($field, $options) {

        $entity = Entity::select()
                        ->where('key', 'field')
                        ->first();

        $capacityType = Type::where('key', 'instance_subject_capacity_type')
                            ->first();

        foreach ($options as $opt) {

            $option = new Option;

            if (isset($opt['id']) && $opt['id']){

                $option = Option::select()->where('id', $opt['id'])->first();

                if (!$option) {
                    continue;
                }
            }

            $option->name = $opt['name'];
            $option->save();

            $entityOptionContext = new EntityOptionContext;
            $entityOptionContext->entity_id = $entity->id;
            $entityOptionContext->instance_id = $field->id;
            $entityOptionContext->type_id = $capacityType->id;
            $entityOptionContext->option_id = $option->id;
            $entityOptionContext->save();
        }

     }



    /**
     * Check if dataset exists
     * @param $name : name of the dataset
     * @param $typeKey : key of the dataset type
     * @return Bool
     */
     protected static function datasetExists ($name, $typeId)
     {

        $count = Dataset::where('name', $name)
                        ->where('type_id', $typeId)
                        ->count();

        return $count > 0;

     }


    /**
     * Check if dataset exists except for where there is a specific ID
     * @param $name : name of the dataset
     * @param $typeKey : key of the dataset type
     * @return Bool
     */
     protected static function datasetExistsExceptForId ($id, $name, $typeId)
     {

        $count = Dataset::where('name', $name)
                        ->where('type_id', $typeId)
                        ->where('id', '!=', $id)
                        ->count();

        return $count > 0;

     }


    /**
     * Get dataset by ID or throw an exception
     * @return Dataset : a found dataset
     */
     protected static function getDatasetByIdOrFail ($id) : Dataset
     {

        $dataset = Dataset::select()
                          ->where('id', $id)
                          ->firstOrFail();

        return $dataset;

     }


     public static function datasetFieldExists ($datasetId, $fieldName) : bool
     {

        $count = Field::join('dataset_field', 'dataset_field.field_id', 'fields.id')
                     ->where('fields.name', $fieldName)
                     ->where('dataset_field.dataset_id', $datasetId)
                     ->count();

        return $count > 0;

     }



     public static function datasetHasField ($datasetId, $fieldId) : bool
     {

        $count = Field::join('dataset_field', 'dataset_field.field_id', 'fields.id')
                     ->where('fields.id', $fieldId)
                     ->where('dataset_field.dataset_id', $datasetId)
                     ->count();

        return $count > 0;

     }



    /**
     * Get dataset with relationships by ID or throw an exception
     * @return Dataset : a found dataset
     */
     protected static function getDatasetWithRelationshipsByIdOrFail ($id) : Dataset
     {

        $dataset = Dataset::select()
                          ->where('id', $id)
                          ->with('fields')
                          ->firstOrFail();

        return $dataset;

     }


     protected static function fieldItemHasOptions($item) : bool
     {
        if (isset($item['options']) && is_array($item['options']) && count($item['options'])) {
            return  true;
        }

        return false;
     }








}
