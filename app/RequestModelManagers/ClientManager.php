<?php

namespace App\RequestModelManagers;


use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use Illuminate\Http\Request;
use App\Models\Type;
use App\Models\Client;
use App\Exceptions\RecordConflictException;
use App\Exceptions\InvalidDataException;


class ClientManager extends RequestModelManager {


    /**
     * Create Client
     * @param $req : Instance of the current request
     * @return Client : created client
     */
    public static function createClient (Request $req) : Client
    {

        $type = Type::select()
                    ->findByIdOrKey($req->type_id, $req->type_key)
                    ->first();

        if (!$type) {
            self::throwInvalidDataException('Type', $req->name);
        }

        $client = new Client;
        $client->name = $req->name;
        $client->type_id = $type->id;
        $client->secret = $req->secret;
        $client->remote_uid = $req->uid;
        $client->save();

        return $client;
    }

    /**
     * Get Clients
     * @param $req : Instance of the current request
     * @return Paginator : a collection of Clients
     */
    public static function getClients (Request $req) : Paginator
    {
       $clients = Client::select();

       if ($req->search_query) {
           $clients->where('name', 'LIKE', "%%{$req->search_query}%%");
       }

       return $clients->paginate();
    }


    /**
     * Get Client
     * @param $req : Instance of the current request
     * @param $id : ID of the  clieent
     * @return Client : found Client
     */
    public static function getClient (Request $req, $id) : Client
    {
       $client = Client::select()
                       ->where('id', $id)
                       ->firstOrFail();
       return $client;
    }


    /**
     * Update Client
     * @param $req : Instance of the current request
     * @param $id : ID of the Client
     * @return Client : updated Client
     */
    public static function updateClient (Request $req, $id) : Client
    {

        $type = Type::select()
                    ->findByIdOrKey($req->type_id, $req->type_key)
                    ->first();

        if (!$type) {
            self::throwInvalidDataException('Type');
        }

        if (self::clientExistsExceptForId($id, $req->name, $type->id)) {
            self::throwRecordConflictException('Client', $req->name);
        }


        $client = Client::select()
                        ->where('id', $id)
                        ->firstOrFail();

        $client->name = $req->name;
        $client->type_id = $type->id;
        $client->secret = $req->secret;
        $client->remote_uid = $req->uid;
        $client->save();

        return $client;
    }

    /**
     * Check if client exists
     * @param $name : name of the client to be checked
     * @return Bool
     */
    protected static function clientExists ($name, $typeId) : bool
    {
        $count = Client::where('name', $name)
                    ->where('type_id', $typeId)
                    ->count();

        return $count > 0;
    }

    /**
     * Check if client exists where ID is not equal to the given $id
     * @param $id : ID of the client
     * @param $name : name of the client to be checked
     * @return Bool
     */
    protected static function clientExistsExceptForId ($id, $name, $typeId) : bool
    {
        $count = Client::where('clients.name', $name)
                        ->where('type_id', $typeId)
                        ->where('id', '!=', $id)
                        ->count();

        return $count > 0;
    }




}
