<?php

namespace App\RequestModelManagers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use App\Models\Entity;

use Cache;


class EntityManager {


    public static function getEntities (Request $req) : Paginator
    {

        $entities =  Entity::selectBasics()
                            ->filterWithTypeIdOrTypeKey($req->type_id, $req->type_key)
                            ->filterWithTypeIdsOrTypeKeys($req->type_ids, $req->type_keys)
                            ->with('activityAccesses')
                            ->paginate();

        return $entities;

    }




}


