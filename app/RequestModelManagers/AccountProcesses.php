<?php

namespace App\RequestModelManagers;

use App\Models\Type;
use App\Models\Platform;
use App\Models\Account;


trait AccountProcesses {


    public static function createNewAccountWithType ($type, $accountData = []) : Account
    {

        $account = new Account;
        $account->type_id = $type->id;
        $account->platform_id = self::getAuthPlatform()->id;
        $account->save();

        return $account;
    }


    public static function createNewAccountWithTypeKey ($typeKey, $accountData = []) : Account
    {
        $type = self::getTypeByKey($typeKey);

        $account = new Account;
        $account->type_id = $type->id;
        $account->platform_id = self::getAuthPlatform()->id;
        $account->save();

        return $account;
    }

    public static function createNewPersonalAccount ($accountData = []) : Account
    {
        return self::createNewAccountWithTypeKey('personal_account_type', $accountData);
    }


    public static function createNewBusinessAccount ($accountData = []) : Account
    {
        return self::createNewAccountWithTypeKey('business_account_type', $accountData);
    }



    public static function createNewOrganizationAccount ($accountData = []) : Account
    {
        return self::createNewAccountWithTypeKey('organization_account_type', $accountData);
    }


    public static function getTypeByKey ($key) : Type
    {
        return Type::select()
                   ->where('key', $key)
                   ->first();
    }

    public static function getAuthPlatform () : Platform
    {
        return RequestModelManager::getAuthPlatform();
    }



}


?>
