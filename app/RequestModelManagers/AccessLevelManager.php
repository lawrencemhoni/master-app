<?php


namespace App\RequestModelManagers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use App\Models\Type;
use App\Models\AccessLevel;

use App\Exceptions\RecordConflictException;
use App\Exceptions\InvalidDataException;

class AccessLevelManager extends RequestModelManager {



    public static function createAccessLevel (Request $req) : AccessLevel
    {
        if (self::accessLevelExists($req->name)) {
            self::throwRecordConflictException('Access Level', $req->name);
        }

        $type = Type::select()
                    ->findByIdOrKey($req->type_id, $req->type_key)
                    ->first();

        if (!$type) {
            self::throwInvalidDataException('Type', '');
        }

        $accessLevel = new AccessLevel;
        $accessLevel->name = $req->name;
        $accessLevel->type_id = $type->id;
        $accessLevel->save();

        return $accessLevel;
    }


    public static function getAccessLevels (Request $req) : Paginator
    {
        $accessLevels = AccessLevel::select()
                                   ->paginate();

        return $accessLevels;
    }

    public static function getAccessLevel(Request $req, $id) : AccessLevel
    {
        return AccessLevel::select()->where('id', $id)->firstOrFail();
    }


    public static function updateAccessLevel (Request $req, $id) : accessLevel
    {

        if (self::accessLevelExistsExceptForId($id, $req->name)) {
            self::throwRecordConflictException('Access Level', $req->name);
        }

        $accessLevel = AccessLevel::select()->where('id', $id)->firstOrFail();

        $type = Type::select()
                    ->findByIdOrKey($req->type_id, $req->type_key)
                    ->first();

        if (!$type) {
            self::throwInvalidDataException('Access Level', $req->name);
        }

        $accessLevel->name = $req->name;
        $accessLevel->type_id = $type->id;
        $accessLevel->save();

        return $accessLevel;

    }


    public static function accessLevelExists ($name) : bool
    {
        $count = AccessLevel::where('name', $name)->count();
        return $count > 0;
    }


    public static function accessLevelExistsExceptForId ($id, $name) : bool
    {
        $count = AccessLevel::where('name', $name)
                            ->where('id', '!=', $id)
                            ->count();
        return $count > 0;

    }









}
