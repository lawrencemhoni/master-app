<?php

namespace App\RequestModelManagers;

use App\Models\User;


trait UserProcesses {

   /**
     * Check if user exists
     * @param $username : username to be checked
     * @return Bool
     */
    protected static function usernameExists ($username) : bool
    {
        $count = User::where('username', $username)->count();
        return $count > 0;

    }


    /**
     * Check if user exists where ID is not equal to the given $id
     * @param $id : ID of the user
     * @param $username : username to be checked
     * @return Bool
     */
    protected static function usernameExistsExceptForId ($id, $username) : bool
    {

        $count = User::where('username', $username)
                    ->where('id', '!=', $id)
                    ->count();

        return $count > 0;

    }


    /**
     * Check if user exists where ID is not equal to the given $id
     * @param $email : email to be checked
     * @return Bool
     */
    protected static function emailExists ($email) : bool
    {

        $count = User::where('email', $email)->count();

        return $count > 0;

    }


    /**
     * Check if user exists where ID is not equal to the given $id
     * @param $id : ID of the user
     * @param $email : email to be checked
     * @return Bool
     */
    protected static function emailExistsExceptForId ($id, $email) : bool
    {

        $count = User::where('email', $email)
                    ->where('id', '!=', $id)
                    ->count();

        return $count > 0;

    }




}
