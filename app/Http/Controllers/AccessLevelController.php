<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Exception;

use App\Exceptions\InvalidDataException;
use App\Exceptions\RecordConflictException;
use App\Exceptions\ForbiddenAccessException;

use App\RequestModelManagers\AccessLevelManager;

use App\Services\EntityAccessService;

class AccessLevelController extends Controller
{




    public function __construct (EntityAccessService $entityAccessServices) {
        $this->entityAccess = $entityAccessServices;
    }

    public function createAccessLevel (Request  $req) {

        try {

            $validated = $req->validate([
                'name' => 'required',
            ]);

            $this->entityAccess->check('access_level',
            'create_activity_type');

            $accessLevel = AccesslevelManager::createAccessLevel($req);

            return response()->json(['data' => $accessLevel], 201);

        }  catch (ValidationException $e) {
            return response()->json([], 422);
        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch (RecordConflictException $e) {
            return response()->json([], 409);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception ) {
            return response()->json([], 500);
        }
    }


    public function getAccessLevels (Request $req) {

        try {

            $this->entityAccess->check('access_level',
            'list_activity_type');

            return AccessLevelManager::getAccessLevels($req);

        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception ) {
            return response()->json([], 500);
        }
    }

    public function viewAccessLevel (Request $req, $id) {

        try {

            $this->entityAccess->check('app',
            'view_activity_type');

            $accessLevel = AccessLevelManager::getAccessLevel($req, $id);

            return response()->json(['data' => $accessLevel], 200);

        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        }  catch ( ModelNotFoundException ) {
            return response()->json([], 404);
        } catch ( Exception ) {
            return response()->json([], 500);
        }
    }

    public function updateAccessLevel (Request $req, $id) {

        try {

            $validated = $req->validate([
                'name' => 'required',
            ]);

            $this->entityAccess->check('access_level',
            'update_activity_type');

            $accessLevel = AccessLevelManager::updateAccessLevel($req, $id);

            return response()->json(['data' => $accessLevel], 200);

        } catch (ValidationException $e) {
            return response()->json([], 422);
        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        }  catch ( ModelNotFoundException ) {
            return response()->json([], 404);
        } catch ( Exception ) {
            return response()->json([], 500);
        }

    }


}
