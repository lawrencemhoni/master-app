<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Exceptions\InvalidDataException;
use App\Exceptions\RecordConflictException;
use App\Exceptions\ForbiddenAccessException;

use App\RequestModelManagers\CategoryManager;

use App\Services\EntityAccessService;

use Exception;


class CategoryController extends Controller
{

    public function __construct (EntityAccessService $entityAccessServices) {
        $this->entityAccess = $entityAccessServices;
    }


    public function createCategory (Request $req) {

        try {

            $validated = $req->validate([
                'name' => 'required',
                'key' => 'required'
            ]);

            $this->entityAccess->check('category',
            'create_activity_type');

            $category = CategoryManager::createCategory($req);

            return response()->json(['data' => $category], 201);

        } catch (ValidationException $e) {
            return response()->json([], 422);
        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch (RecordConflictException $e) {
            return response()->json([], 409);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception $e ) {
            \Log::info($e->getMessage());
            return response()->json([], 500);
        }

    }


    public function getCategories (Request $req) {

        try {

            $this->entityAccess->check('category',
            'list_activity_type');

            return CategoryManager::getCategories($req);

        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception $e ) {
            return response()->json([], 500);
        }

    }



    public function viewCategory (Request $req, $id) {

        try {

            $this->entityAccess->check('category',
            'view_activity_type');

            $category = CategoryManager::getCategory($req, $id);

            return response()->json(['data' => $category], 200);


        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception  $e) {
            return response()->json([], 500);
        }
    }



    public function updateCategory (Request $req, $id) {

        try {

            $validated = $req->validate([
                'name' => 'required',
                'key' => 'required'
            ]);

            $this->entityAccess->check('category',
            'update_activity_type');

            $category =  CategoryManager::updateCategory($req, $id);

            return response()->json(['data' => $category], 200);

        } catch (ValidationException $e) {
            return response()->json([], 422);
        }  catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        }  catch ( ModelNotFoundException $e) {
            return response()->json([], 404);
        } catch ( Exception $e) {
            \Log::info($e->getMessage());
            return response()->json([], 500);
        }

    }




}
