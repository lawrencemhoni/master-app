<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Exceptions\InvalidDataException;
use App\Exceptions\RecordConflictException;
use App\Exceptions\ForbiddenAccessException;

use App\RequestModelManagers\EntitManager;
use App\RequestModelManagers\EntityDatasetManager;

use App\Services\EntityAccessService;

use Exception;

use App\RequestModelManagers\EntityManager;


class EntityController extends Controller
{
    public function __construct (EntityAccessService $entityAccessServices) {
     $this->entityAccess = $entityAccessServices;
    }

    public function getEntities (Request $req) {

        try {


            $this->entityAccess->check('entity', 'list_activity_type');

            return EntityManager::getEntities($req);

        } catch (ValidationException $e) {
            return response()->json([], 422);
        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch (RecordConflictException $e) {
            return response()->json([], 409);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception $e ) {
            \Log::info($e->getMessage());
            return response()->json([], 500);
        }



    }




}
