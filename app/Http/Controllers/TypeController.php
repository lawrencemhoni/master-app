<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exceptions\InvalidDataException;
use App\Exceptions\RecordConflictException;
use App\Exceptions\ForbiddenAccessException;

use App\RequestModelManagers\TypeManager;

use App\Services\EntityAccessService;


class TypeController extends Controller
{
    public function __construct (EntityAccessService $entityAccessServices) {
        $this->entityAccess = $entityAccessServices;
    }

    public function createType (Request $req) {

        try {

            $validated = $req->validate([
                'name' => 'required'
            ]);

            $this->entityAccess->check('type',
            'create_activity_type');

            $type = TypeManager::createType($req);

            return response()->json(['data' => $type], 201);

        } catch (ValidationException $e) {
            return response()->json([], 422);
        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch (RecordConflictException $e) {
            return response()->json([], 409);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception ) {
            return response()->json([], 500);
        }

    }


    public function getTypes (Request $req) {

        try {

            $this->entityAccess->check('type',
            'list_activity_type');

            return TypeManager::getTypes($req);

        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        } catch ( Exception ) {
            return response()->json([], 500);
        }

    }


    public function viewType (Request $req, $id) {

        try {

            $this->entityAccess->check('type',
            'view_activity_type');

            $type = TypeManager::getType($req, $id);

            return response()->json(['data' => $type], 200);

        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        }  catch ( ModelNotFoundException $e ) {
            return response()->json([], 404);
        } catch ( Exception $e) {
            return response()->json([], 500);
        }
    }

    public function updateType (Request $req, $id) {

        try {

            $validated = $req->validate([
                'name' => 'required',
            ]);

            $this->entityAccess->check('type',
            'update_activity_type');

            $type = TypeManager::updateType($req, $id);

            return response()->json(['data' => $type], 200);

        }  catch (ValidationException $e) {
            return response()->json([], 422);
        } catch (ForbiddenAccessException $e) {
            return response()->json([], 403);
        } catch ( InvalidDataException $e ) {
            return response()->json([], 400);
        }  catch ( ModelNotFoundException $e ) {
            return response()->json([], 404);
        } catch ( Exception $e ) {
            return response()->json([], 500);
        }

    }





}
