<?php

namespace App\Services;

use App\Exceptions\ForbiddenAccessException;

use App\Models\EntityActivityAccess;
use Auth;

class EntityAccessService {


	public function check ($entityKey, $activityTypeKey) {

        $userId = Auth::user()->id;

		$count = EntityActivityAccess::select()
                    ->join('user_entity_activity_access',
                        'user_entity_activity_access.entity_activity_access_id',
                        'entity_activity_accesses.id'
                    )
                    ->join('entities', 'entities.id', 'entity_activity_accesses.entity_id')
                    ->join('types', 'types.id', 'entity_activity_accesses.type_id')
					->where('user_id', $userId)
                    ->where('entities.key', $entityKey)
					->where('types.key', $activityTypeKey)
					->count();


		if ($count > 0) {
			return true;
		}

		throw new ForbiddenAccessException("Unauthorized Access");
	}



}
